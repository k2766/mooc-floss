# Gitlab server setup
The following instructions assume you are setting up your server on OpenStack.

Copy `secrets.auto.tfvars.example` to `secrets.auto.tfvars` and fill the missing variables.
For this project, they are located under Settings > CI/CD > Variables.

Copy `secrets.auto.tfvars` into `infranstructure/terraform/gitlab`

**Note** The `.pem` file for the value set in `openstack_gitlab_keypair_name` needs to have been copied somewhere 
safe for use by `ansible-playbook`. Remember to give the file appropriate permissions e.g `chown 400 key.pem`

Before initializing terraform, you need to have a [GitLab access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) with the `api` scope.
Once you have the access token, set it as an environment variable because it's going to be used later i.e

```bash
export TF_PASSWORD="<gitlab-personal-access-token>"
```
also, set the following variables:
```bash
export PROJECT_ID="23931551"
export TF_USERNAME="<gitlab-username>"
export TF_ADDRESS="https://gitlab.com/api/v4/projects/${PROJECT_ID}/terraform/state/gitlab"
```
Finally, in `infranstructure/terraform/gitlab`, initialize terraform with:
```bash
terraform init \
  -backend-config=address=${TF_ADDRESS} \
  -backend-config=lock_address=${TF_ADDRESS}/lock \
  -backend-config=unlock_address=${TF_ADDRESS}/lock \
  -backend-config=username=${TF_USERNAME} \
  -backend-config=password=${TF_PASSWORD} \
  -backend-config=lock_method=POST \
  -backend-config=unlock_method=DELETE \
  -backend-config=retry_wait_min=5
```
Now, you can run:
```bash
terraform plan
```
If everything looks good:
```bash
terraform apply
```

Once the server has been deployed, you'll see the ip address of like so:
```bash
....
Apply complete! Resources: 2 added, 0 changed, 0 destroyed.

Outputs:

gitlab_instance_ip = "147.135.195.204"

```

To see the IP address of a server that has already been deployed, run:

```bash
terraform show
```

**Note:**
If you are planning on using the ansible playbook to set up Gitlab, you need to have a valid
domain/subdomain pointing to the produced IP address.
