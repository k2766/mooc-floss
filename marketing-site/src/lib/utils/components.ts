import {assets} from '$app/paths';

function resolveStaticAssetPath(pathname: string): string {
  return `${assets}/${pathname}`;
}

function getModifierClassName(base: string, modifiers: string[]): string {
  return modifiers
    .filter(Boolean)
    .map((m) => `${base}--${m}`)
    .join(' ');
}

export {getModifierClassName, resolveStaticAssetPath};
