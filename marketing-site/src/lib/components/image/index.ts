import CaptionedImage from './captioned-image.svelte';
import Avatar from './avatar.svelte';

export {Avatar, CaptionedImage};
