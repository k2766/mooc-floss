import {resolve, dirname} from 'path';
import {fileURLToPath} from 'url';

import svg from '@poppanator/sveltekit-svg';
import {extendDefaultPlugins as svgoExtendDefaultPlugins} from 'svgo';
import preprocess from 'svelte-preprocess';
import adapter from '@sveltejs/adapter-static';
import autoprefixer from 'autoprefixer';
import postcssCustomProperties from 'postcss-custom-properties';
import postcssLogical from 'postcss-logical';

const __dirname = dirname(fileURLToPath(import.meta.url));

/** @type {import('@sveltejs/kit').Config} */
const config = {
  preprocess: preprocess({
    defaults: {
      style: 'scss',
    },

    scss: {
      includePaths: ['src', 'node_modules/normalize.css'],
    },

    postcss: {
      plugins: [postcssCustomProperties, postcssLogical({dir: 'ltr'}), autoprefixer],
    },
  }),

  kit: {
    // hydrate the <div id="svelte"> element in src/app.html
    target: '#svelte',

    paths: {
      base: process.env.VITE_BASE_PATH || '',
    },

    adapter: adapter({
      pages: 'build',
      assets: 'build',
      fallback: null,
    }),

    vite: () => ({
      plugins: [
        svg({
          svgoOptions: {
            plugins: svgoExtendDefaultPlugins([
              /**
               * Delete id from SVG elements to fix accessibility issues of duplicate
               * ids in the document
               */
              {
                name: 'customRemoveSvgId',
                type: 'perItem',
                fn: (ast) => {
                  [ast]
                    .filter(({name}) => name === 'svg')
                    .filter(({attributes}) => Boolean(attributes))
                    .map(({attributes}) => delete attributes.id);
                },
              },

              /**
               * Add width attribute to SVG elements to render SVGs using their
               * native dimensions
               */
              {
                name: 'customAddWidthToSvg',
                type: 'perItem',
                fn: (ast) => {
                  [ast]
                    .filter(({name}) => name === 'svg')
                    .filter(({attributes}) => Boolean(attributes))
                    .map(({attributes}) => {
                      const {viewBox} = attributes;
                      const [, , width] = viewBox ? viewBox.split(' ') : Array(4);

                      attributes.width = width ? `${width}px` : undefined;
                    });
                },
              },

              /**
               * Don't merge paths of SVGs - this is required for the sparkly animated
               * SVGs to have each individual line animated
               */
              {
                name: 'mergePaths',
                active: false,
              },
            ]),
          },
        }),
      ],
      resolve: {
        alias: {
          $components: resolve(__dirname, './src/lib/components'),
          $data: resolve(__dirname, './src/lib/data'),
          $utils: resolve(__dirname, './src/lib/utils'),
          $assets: resolve(__dirname, './src/assets'),
        },
      },
    }),
  },
};

export default config;
