Marc Jeanmougin:https://mensuel.framapad.org/p/mooc-floss-0-9lpt?lang=en
Olivier Berger (IMT):Loïc, remember that ? : https://pics.fsfe.org/uploads/medium/a37dbb0fcf678fa265ab0dba12412095.jpeg
Olivier Berger (IMT):context: https://fsfe.org/news/2021/news-20210204-01.en.html
loic:no :-)
Olivier Berger (IMT):@loic : URL of the course ?
Rémi SHARROCK:Anna lay you mute your mic ?
Rémi SHARROCK:may*
Rémi SHARROCK:thanks
Anna Khazina:sorry
loic:it is defunct. The OpenStack course is at https://docs.openstack.org/upstream-training/
Olivier Berger (IMT):I just started in the late 90's, whereas Loic started in the 80's
Rémi SHARROCK:haha
Olivier Berger (IMT):spreading tapes filled with FLOSS
Olivier Berger (IMT):(loic)
Xavier Antoviaque::p
Olivier Berger (IMT):The goal of this week is to discuss Organizations, people and licenses that are already around in the FLOSS ecosystem, such as small projects, non-profits (for FLOSS in general, or orgs geared toward a specific project, or umbrella orgs) Companies (centered around a specific project, or companies opensourcing part of their work) Economic models, sustainability, CLA/DCO, "free work" Licenses, their differences, what this implies
Marc Jeanmougin:"at the end of this week, you sort of need to have found a project by now and have a good idea that you will do something with them"
polyedre:Companies ?
loic:https://www.wikidata.org/wiki/Wikidata:WikiProject_Informatics/FLOSS just pops in my mind (not sure how it could be used but ...)
Olivier Berger (IMT):;-)
Olivier Berger (IMT):I've got BBB messages about joining audio :-/
polyedre:I agree :)
Marc Jeanmougin:but let's not start with Elastic as an example :p
polyedre:Yes !
Marc Jeanmougin:https://media.ccc.de/v/36c3-10875-hack_curio
polyedre:I don't know the difference between licences related to code (GPL, etc) vs licences about content (CC-By-*), maybe it would be interesting to add this to the Mooc ?
Rémi SHARROCK:absolutely
loic:+1
polyedre:Yes
polyedre:https://choosealicense.com
Rémi SHARROCK:https://creativecommons.org/choose/
Marc Jeanmougin:https://creativecommons.org/choose/
Rémi SHARROCK:avant :) NA
Olivier Berger (IMT):like gforge
loic:*very* confusing business model indeed :-)
loic:gforge renaming is even more confusing
Olivier Berger (IMT):is it still a thing ?
loic:yes, a proprietary thing
Olivier Berger (IMT):crowdsourcing
